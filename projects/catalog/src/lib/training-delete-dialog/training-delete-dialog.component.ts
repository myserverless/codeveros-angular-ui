import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Training} from '../training.interface';

@Component({
  templateUrl: './training-delete-dialog.component.html',
  styleUrls: [ './training-delete-dialog.component.scss' ]
})
export class TrainingDeleteDialogComponent {
  training: Training;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<TrainingDeleteDialogComponent>
  ) {
    if (data && data.training) {
      this.training = data.training;
    }
  }
}
